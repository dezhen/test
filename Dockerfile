FROM dezhen/nginx:test

MAINTAINER dz <dezhen98@gmail.com>

CMD ["/usr/sbin/nginx","-g","daemon off;"]

EXPOSE 80 443
